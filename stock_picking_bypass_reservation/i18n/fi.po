# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* stock_picking_bypass_reservation
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 12.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-06 05:22+0000\n"
"PO-Revision-Date: 2020-04-06 08:25+0300\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: \n"
"Language: es\n"
"X-Generator: Poedit 2.0.6\n"

#. module: stock_picking_bypass_reservation
#: model:ir.model.fields,field_description:stock_picking_bypass_reservation.field_stock_location__allow_bypass_reservation
msgid "Always allow stock moves"
msgstr "Salli varastosiirrot aina"

#. module: stock_picking_bypass_reservation
#: model:ir.model,name:stock_picking_bypass_reservation.model_stock_location
msgid "Inventory Locations"
msgstr "Inventaariopaikat"

#. module: stock_picking_bypass_reservation
#: model:ir.model.fields,help:stock_picking_bypass_reservation.field_stock_location__allow_bypass_reservation
msgid ""
"When selected, always allow stock moves on this location for every\n"
"        product. Negative stock quants are permitted because of this."
msgstr ""
"Salli varastosiirrot kaikille tuotteille tässä varastopaikassa.\n"
"\tNegatiiviset hankintaerien määrät on täten sallittu."

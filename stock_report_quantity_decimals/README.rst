.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

================================================================
Quantity Decimals Precision on Picking and Delivery Slip reports
================================================================

* Change the number of decimals shown on Stock reports' product quantities

Configuration
=============
* Stock reports' decimals precision of product quantities can be set by going
  to Inventory --> Configuration --> Settings and changing the value under the
  stock_report_decimal_precision-field. The integer represents the number of
  decimals shown on product quantities.

Usage
=====
* Install the module from Apps

Known issues / Roadmap
======================
* None and this module works with multi-companies also.

Credits
=======

Contributors
------------

* Timo Kekäläinen <timo.kekalainen@tawasta.fi>

Maintainer
----------

.. image:: http://tawasta.fi/templates/tawastrap/images/logo.png
   :alt: Oy Tawasta OS Technologies Ltd.
   :target: http://tawasta.fi/

This module is maintained by Oy Tawasta OS Technologies Ltd.
